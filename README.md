
# Assembleia de Votação

## 1. Resumo
No cooperativismo, cada associado possui um voto e as decisões são tomadas em assembleias, por votação. A partir disso, você precisa criar uma solução back-end para gerenciar essas sessões de votação. 

## 2. Requisitos Funcionais
Essa solução deve ser executada na nuvem e promover as seguintes funcionalidades através de uma API REST:

- Cadastrar uma nova pauta;
- Abrir uma sessão de votação em uma pauta (a sessão de votação deve ficar aberta por um tempo determinado na chamada de abertura ou 1 minuto por default);
- Receber votos dos associados em pautas (os votos são apenas 'Sim'/'Não'. Cada associado é identificado por um id único e pode votar apenas uma vez por pauta);
- Contabilizar os votos e dar o resultado da votação na pauta.

Para fins de exercício, a segurança das interfaces pode ser abstraída e qualquer chamada para as interfaces pode ser considerada como autorizada. A escolha da linguagem, frameworks e bibliotecas é livre (desde que não infrinja direitos de uso).

É importante que as pautas e os votos sejam persistidos e que não sejam perdidos com o restart da aplicação.

## 3. Dependências

O que você precisa para configurar e executar esta aplicação no ambiente local?

- [Java 11](https://www.oracle.com/java/technologies/javase-jdk11-downloads.html)
- [Eclipse for EE Developers](https://www.eclipse.org/downloads/packages/) ou [STS Studio 4](https://spring.io/tools)
- [Git SCM](https://git-scm.com/downloads)
- SourceTree

---

## 4. Tecnologias usadas no Projeto

Neste projeto foram utilizadas as seguintes tecnologias/frameworks.

- **Spring** e seus módulos (Spring Boot, Spring Cloud, Spring JPA, etc.) como base para construção do projeto;
- **Feign** para integração com APIs externas;
- **H2 Database** como banco de dados para testes e validar a aplicação;
- **Lombok** para processamento de classes através das anotações;
- **Swagger** para documentação e consulta das APIs;
- **GitFlow** para organização e gerenciamento de versão do GIT.

---

## 5. Executando o Projeto

As configurações necessárias para levantar esse projeto no ambiente local não exige passos específicos. A IDE configurada no máquina de desenvolvimento deve apenas sincronizar as dependências do sistema e executar a aplicação como **Spring Boot App**

## Referências

Alguns artigos que foram usados como referência para estudo e construção desse projeto:

1. [Construindo uma arquitetura corporativa de alto nível com a Onion Architecture](https://www.infoq.com/br/articles/onion-architecture/);
2. [Fluxo de trabalho de Gitflow](https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow)

## Sobre o Autor

Tarcísio de Lima Amorim <br>
Email: tarcisio.lima.amorim@outlook.com <br>
Linkedin: https://www.linkedin.com/in/tarcisio-lima/