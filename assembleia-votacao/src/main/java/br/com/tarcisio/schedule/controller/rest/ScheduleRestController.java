package br.com.tarcisio.schedule.controller.rest;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tarcisio.schedule.application.dto.ScheduleRequest;
import br.com.tarcisio.schedule.application.dto.ScheduleResponse;
import br.com.tarcisio.schedule.application.dto.ScheduleSessionRequest;
import br.com.tarcisio.schedule.application.service.interfaces.IScheduleService;
import br.com.tarcisio.schedule.domain.entity.enums.ScheduleResultTypeEnum;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;

/**
 * Controller da pauta
 * @author Tarcísio Lima
 */
@RequestMapping("/v1/schedules")
@RestController
@RequiredArgsConstructor
@Api("Endpoints para o recurso das pautas")
public class ScheduleRestController {

	private final IScheduleService scheduleService;
	
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<ScheduleResponse>> findAll(@RequestParam(required = false, defaultValue = "ASC") String sort) {
		
        var schedules = scheduleService.findAll(Sort.by(Sort.Direction.fromString(sort), "title"));
        
        return schedules.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(schedules, HttpStatus.OK);
    }
	
	/**
	 * Cadastra uma nova pauta
	 * @param pautaRequest
	 * @return
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ScheduleResponse> addSchedule(@RequestBody ScheduleRequest scheduleRequest) {
		return new ResponseEntity<>(scheduleService.addSchedule(scheduleRequest), HttpStatus.CREATED);
	}
	
	/**
	 * Inicia, caso não ocorra nenhum impedimento uma nova sessão de votação para uma pauta 
	 * (Patch atualiza parcialmente os recursos da schedule)
	 * @param id
	 * @param schedule
	 * @return
	 */
	@PatchMapping(
			value = "/{scheduleId}/start-session", 
			produces = MediaType.APPLICATION_JSON_VALUE, 
			consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ScheduleResponse> startSession(
    		@PathVariable Long scheduleId, @RequestBody(required = false) ScheduleSessionRequest request) {
		
        return new ResponseEntity<>(scheduleService.startSession(scheduleId, request), HttpStatus.OK);
    }

	/**
	 * Traz o resultado da votação da pauta
	 * @param scheduleId
	 * @return
	 */
    @GetMapping(value = "/{scheduleId}/result", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ScheduleResultTypeEnum> getResult(@PathVariable Long scheduleId) {
    	
        return new ResponseEntity<>(scheduleService.getResult(scheduleId), HttpStatus.OK);
    }

}
