package br.com.tarcisio.schedule.domain.entity;

import java.io.Serializable;
import java.time.LocalTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import br.com.tarcisio.schedule.domain.entity.enums.ScheduleStatusEnum;
import br.com.tarcisio.voting.domain.entity.Vote;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Representa a pauta
 * @author Tarcísio Lima
 */
@Entity
@Table(name = "EN_SCHEDULE", schema = "ASSEMBLY")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Schedule implements Serializable{

	private static final long serialVersionUID = 1L;

	public Schedule(Long id) {
		super();
		this.id = id;
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "TITLE", nullable = false, length = 100)
	private String title;
	
	@Column(name = "DESCRIPTION", nullable = true, length = 255)
	private String description;
	
	@Column(name = "START_TIME")
	private LocalTime startTime;
	
	@Column(name = "END_TIME")
	private LocalTime endTime;
	
	@Enumerated(EnumType.STRING)
    @Column(name="STATUS", nullable = false)
    private ScheduleStatusEnum status;
	
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "schedule", fetch = FetchType.EAGER)
	private List<Vote> votes;
	
}
