package br.com.tarcisio.schedule.application.service;

import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityNotFoundException;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.tarcisio.infrastructure.enums.ErrorMessagesEnum;
import br.com.tarcisio.infrastructure.exception.BusinessException;
import br.com.tarcisio.schedule.application.dto.ScheduleRequest;
import br.com.tarcisio.schedule.application.dto.ScheduleResponse;
import br.com.tarcisio.schedule.application.dto.ScheduleSessionRequest;
import br.com.tarcisio.schedule.application.service.interfaces.IScheduleService;
import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.schedule.domain.entity.enums.ScheduleResultTypeEnum;
import br.com.tarcisio.schedule.domain.entity.enums.ScheduleStatusEnum;
import br.com.tarcisio.schedule.domain.repository.ScheduleRepository;
import br.com.tarcisio.voting.application.service.interfaces.IVoteService;
import br.com.tarcisio.voting.domain.entity.enums.VoteTypeEnum;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@AllArgsConstructor
@Slf4j
public class ScheduleService implements IScheduleService {
	
	private final ScheduleRepository scheduleRepository;
	private final IVoteService voteService;
	private final Long DEFAULT_MINUTE = 1L;

	/**
	 * Implementação do método que insere uma nova pauta
	 */
	@Override
	public ScheduleResponse addSchedule(ScheduleRequest request) {
		
		log.info("Cadastrando nova pauta: "+request.toString());
				
		return new ScheduleResponse(scheduleRepository.save(request.toEntity()));
	}

	/**
	 * Implementação do método que abre uma nova sessão de votação para uma pauta
	 */
	@Override
	public ScheduleResponse startSession(Long scheduleId, ScheduleSessionRequest request) {
		
		log.info("Iniciando nova sessão de votação para a pauta -> {}", scheduleId);
				
		var schedulePatched = this.patchScheduleWithNewSession(scheduleId, request);		
		
		return new ScheduleResponse(scheduleRepository.save(schedulePatched));
	}

	/**
	 * Valida e atualiza os membros parciais correspondente a abertura da sessão na pauta
	 * @param scheduleId
	 * @param request
	 * @return
	 */
	private Schedule patchScheduleWithNewSession(Long scheduleId, ScheduleSessionRequest request) {
		
		log.info("Valiando parâmetros e definindo período da sessão");
		
		var startTime = LocalTime.now();
		var endTime = request != null && request.getSessionTimeInMinutes() > DEFAULT_MINUTE ? 
				startTime.plusMinutes(request.getSessionTimeInMinutes()) : 
					startTime.plusMinutes(DEFAULT_MINUTE);
		
		var scheduleToPatch = scheduleRepository.findById(scheduleId).orElseThrow(EntityNotFoundException::new);
		
		if(scheduleToPatch.getStartTime() == null && scheduleToPatch.getEndTime() == null) {
			scheduleToPatch.setStartTime(startTime);
			scheduleToPatch.setEndTime(endTime);
			scheduleToPatch.setStatus(ScheduleStatusEnum.OPEN);
			
		} else 
			throw new BusinessException(ErrorMessagesEnum.SESSION_ALREADY_EXISTS);
		
		log.info("Sessão definida: Início -> {}; Fim -> {}", startTime.toString(), endTime.toString());
		
		return scheduleToPatch;
	}

	/**
	 * Busca todos as pautas
	 */
	@Override
	public List<ScheduleResponse> findAll(Sort sort) {
		
		log.info("Buscando todas pautas por ordem = "+sort.toString());
		
		return scheduleRepository.findAll(sort)
				.stream()
				.map(ScheduleResponse::new)
				.collect(Collectors.toList());
	}

	/**
	 * Retorna o resultado da votação da pauta
	 */
	@Override
	public ScheduleResultTypeEnum getResult(Long scheduleId) {
		
		var scheduleToCalculate = scheduleRepository.findById(scheduleId);
		
		if(scheduleToCalculate.isPresent()) {
			
			var votesCount = voteService.countVotesFromSchedule(scheduleToCalculate.get());
			
			return scheduleSessionIsFinished(scheduleToCalculate.get()) ? 
					calculateResultFromVotes(votesCount) :
					ScheduleResultTypeEnum.UNDEFINED;
			
		} else throw new BusinessException(ErrorMessagesEnum.SCHEDULE_NOT_FOUND);
	}
	
	/**
	 * Calcula o resultado da votação da pauta
	 * @param votesCount
	 * @return
	 */
	private ScheduleResultTypeEnum calculateResultFromVotes(Map<VoteTypeEnum, Long> votesCount) {
		
		if (votesCount.get(VoteTypeEnum.YES).equals(votesCount.get(VoteTypeEnum.NO))) {
			return ScheduleResultTypeEnum.DRAW;
			
		}else if (votesCount.get(VoteTypeEnum.YES) > votesCount.get(VoteTypeEnum.NO)){
			return ScheduleResultTypeEnum.APPROVED;
					
		}else {
			return ScheduleResultTypeEnum.DISAPPROVED;
		}
	}
	
	/**
	 * Verifica se a sessão se a sessão foi finalizada
	 * @param schedule
	 * @return
	 */
	private boolean scheduleSessionIsFinished(Schedule schedule) {
		
		if(schedule.getStatus() == ScheduleStatusEnum.OPEN && 
				LocalTime.now().isAfter(schedule.getEndTime())) {
			
			log.info("Atualizando status da pauta para finalizado.");
			
			scheduleRepository.updateScheduleStatusFinished(schedule.getId());
			schedule.setStatus(ScheduleStatusEnum.FINISHED);
		}
		
		return schedule.getStatus() == ScheduleStatusEnum.FINISHED;
	}

}
