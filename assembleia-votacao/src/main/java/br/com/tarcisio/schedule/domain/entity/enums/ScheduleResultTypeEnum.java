package br.com.tarcisio.schedule.domain.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Lista de possíveis resultados de votação da pauta
 * @author Tarcísio Lima
 *
 */
@Getter
@AllArgsConstructor
public enum ScheduleResultTypeEnum {
	
    APPROVED("Approved"),
    DISAPPROVED("Disapproved"),
    UNDEFINED("Undefined"),
    DRAW("Draw");

    String resultType;
}
