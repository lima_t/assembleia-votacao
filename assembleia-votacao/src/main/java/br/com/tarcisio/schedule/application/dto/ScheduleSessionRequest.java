package br.com.tarcisio.schedule.application.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Objeto representacional de entrada da sessão da pauta
 * @author Tarcísio Lima
 */
@Data
@NoArgsConstructor
public class ScheduleSessionRequest {

	private Integer sessionTimeInMinutes;

}
