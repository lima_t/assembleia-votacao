package br.com.tarcisio.schedule.application.dto;

import java.time.LocalTime;

import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.schedule.domain.entity.enums.ScheduleStatusEnum;
import lombok.Data;

/**
 * Objeto representacional de saída da pauta
 * @author Tarcísio Lima
 */
@Data
public class ScheduleResponse {

	private Long id;
	private String title;
	private String description;
	private LocalTime startTime;
	private LocalTime endTime;
	private ScheduleStatusEnum status;
	
	/**
	 * Construtor de entity em objeto representacional de saída
	 * @param schedule
	 */
	public ScheduleResponse(Schedule schedule) {
		this.id = schedule.getId();
		this.title = schedule.getTitle();
		this.description = schedule.getDescription();
		this.startTime = schedule.getStartTime();
		this.endTime = schedule.getEndTime();
		this.status = schedule.getStatus();
	}
	
}
