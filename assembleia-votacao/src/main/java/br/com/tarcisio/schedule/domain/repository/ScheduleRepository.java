package br.com.tarcisio.schedule.domain.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.tarcisio.schedule.domain.entity.Schedule;

public interface ScheduleRepository extends JpaRepository<Schedule, Long> {

	@Modifying
	@Transactional
	@Query("UPDATE Schedule s SET s.status = 'FINISHED' WHERE s.id = :scheduleId")
	void updateScheduleStatusFinished(@Param("scheduleId") Long scheduleId);
	
}
