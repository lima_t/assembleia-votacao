package br.com.tarcisio.schedule.domain.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Lista de status da pauta
 * @author Tarcísio Lima
 *
 */
@AllArgsConstructor
@Getter
public enum ScheduleStatusEnum {

	CREATED("Created"),
	OPEN("Open"),
	FINISHED("Finished");
	
	private String status;
	
}
