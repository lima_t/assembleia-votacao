package br.com.tarcisio.schedule.application.service.interfaces;

import java.util.List;

import org.springframework.data.domain.Sort;

import br.com.tarcisio.schedule.application.dto.ScheduleRequest;
import br.com.tarcisio.schedule.application.dto.ScheduleResponse;
import br.com.tarcisio.schedule.application.dto.ScheduleSessionRequest;
import br.com.tarcisio.schedule.domain.entity.enums.ScheduleResultTypeEnum;

/**
 * Interface pauta
 * @author Tarcísio Lima
 */
public interface IScheduleService {
	
	/**
	 * Template buscar todas as pautas
	 * @param sort
	 * @return
	 */
	List<ScheduleResponse> findAll(Sort sort);
	
	/**
	 * Template método adicionar pauta
	 * @param request
	 * @return
	 */
	ScheduleResponse addSchedule(ScheduleRequest request);
	
	/**
	 * Template método que inicia uma sessão de votação para uma pauta
	 * @param id
	 * @param scheduleSessionDto
	 * @return
	 */
	ScheduleResponse startSession(Long scheduleId, ScheduleSessionRequest request);
	
	/**
	 * Template método que calcula o resultado da sessão
	 * @param scheduleId
	 * @return
	 */
	ScheduleResultTypeEnum getResult(Long scheduleId);
	
}
