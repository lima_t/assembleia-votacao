package br.com.tarcisio.schedule.application.dto;

import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.schedule.domain.entity.enums.ScheduleStatusEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Objeto representacional de entrada da pauta
 * @author Tarcísio Lima
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class ScheduleRequest {

	private String title;
	private String description;
	
	/**
	 * Transforma o request objeto representacional para entity
	 * @param request
	 * @return
	 */
	public Schedule toEntity() {
		return new Schedule(
				null, 
				title, 
				description,
				null,
				null,
				ScheduleStatusEnum.CREATED,
				null);
	}
	
}
