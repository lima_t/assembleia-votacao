package br.com.tarcisio.infrastructure.exception.dto;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class ExceptionResponse {

	private String applicationName;
	private String errorMessage;
	
}
