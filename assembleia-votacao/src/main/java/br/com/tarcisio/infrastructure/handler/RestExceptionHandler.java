package br.com.tarcisio.infrastructure.handler;

import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.tarcisio.infrastructure.exception.BusinessException;
import br.com.tarcisio.infrastructure.exception.dto.ExceptionResponse;
import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Classe interceptadora das exceções da aplicação
 * @author Tarcísio Lima
 *
 */
@RestControllerAdvice
@AllArgsConstructor
@Slf4j
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

	private final Environment environment;
	
	/**
	 * Erros gerais
	 * @param e
	 * @return
	 */
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<Object> errorGenericException(Exception e) {
		
        log.error("Erro inesperado na aplicação → " + e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
        		.body(
        				ExceptionResponse.builder()
        				.applicationName(environment.getProperty("spring.application.name"))
        				.errorMessage(e.getMessage())
        				.build());
    }
	
	/**
	 * Erros de negócio
	 * @param e
	 * @return
	 */
	@ResponseStatus(value = HttpStatus.UNPROCESSABLE_ENTITY)
    @ExceptionHandler(BusinessException.class)
    @ResponseBody
    public ResponseEntity<Object> errorScheduleException(BusinessException e) {
		
        log.error("Erro de negócio → " + e.getMessage(), e);
        return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
        		.body(
        				ExceptionResponse.builder()
        				.applicationName(environment.getProperty("spring.application.name"))
        				.errorMessage(e.getMessage())
        				.build());
    }
	
	/**
	 * Erro do Feign
	 * @param e
	 * @return
	 */
	@ExceptionHandler(FeignException.class)
    @ResponseBody
    public ResponseEntity<Object> errorFeignException(FeignException e) {
		
        log.error("Erro ao chamar API externa via Feign → " + e.getMessage(), e);
        return ResponseEntity.status(e.status())
        		.body(
        				ExceptionResponse.builder()
        				.applicationName(environment.getProperty("spring.application.name"))
        				.errorMessage(e.getMessage())
        				.build());
    }
	
}
