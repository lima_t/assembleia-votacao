package br.com.tarcisio.infrastructure.exception;

import br.com.tarcisio.infrastructure.enums.ErrorMessagesEnum;

/**
 * Exceção base para erros na pauta
 * @author Tarcísio Lima
 *
 */
public class BusinessException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	
	/**
	 * Construtor com mensagens personalizadas
	 * @param errorMessage
	 */
	public BusinessException(ErrorMessagesEnum errorMessage) {
		super(errorMessage.getMessageValue());
	}
	
}
