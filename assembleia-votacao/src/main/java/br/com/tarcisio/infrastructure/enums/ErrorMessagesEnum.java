package br.com.tarcisio.infrastructure.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Lista de mensagens de erros associadas ao sistema
 * @author Tarcísio Lima
 *
 */
@AllArgsConstructor
@Getter
public enum ErrorMessagesEnum {
	
	ASSOCIATED_CAN_NOT_VOTE("Associated can't vote"),
	ASSOCIATED_NOT_FOUND("Associated not found"),
	SCHEDULE_NOT_FOUND("Schedule not found"),
	SCHEDULE_ALREADY_FINISHED("Schedule session already finished"),
	SESSION_ALREADY_EXISTS("Schedule session already exists");
	
	private String messageValue;
	
}
