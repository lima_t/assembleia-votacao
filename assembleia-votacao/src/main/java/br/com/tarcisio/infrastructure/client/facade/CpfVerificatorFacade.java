package br.com.tarcisio.infrastructure.client.facade;

import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import br.com.tarcisio.infrastructure.client.interfaces.ICpfVerificatorClient;
import br.com.tarcisio.infrastructure.enums.AbleToVoteTypeEnum;
import br.com.tarcisio.infrastructure.model.AbleToVoteModel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Fachada para chamada da api externa que verifica se o CPF é valido ou não
 * @author Tarcísio Lima
 *
 */
@Service
@AllArgsConstructor
@Slf4j
public class CpfVerificatorFacade {
	
	private final ICpfVerificatorClient cpfVerificatorClient;
	private final Environment environment;

	/**
	 * Verifica se o CPF é valido ou não
	 * @param cpf
	 * @return
	 */
	public boolean isValidCPF(String cpf) {
		
		log.info("Consultando API externa via Feign -> {}", environment.getProperty("heroku-api.url"));
		
		AbleToVoteModel canVoteByValidCPF = cpfVerificatorClient.isAbleToVoteByCPF(cpf);
		
		log.info("API externa retornou {} para o CPF {}", canVoteByValidCPF.getAbleToVoteType(), cpf);
		
        return canVoteByValidCPF.getAbleToVoteType() == AbleToVoteTypeEnum.ABLE_TO_VOTE;
	}
	
}
