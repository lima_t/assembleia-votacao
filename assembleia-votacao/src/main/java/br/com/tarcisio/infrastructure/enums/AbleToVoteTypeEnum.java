package br.com.tarcisio.infrastructure.enums;

import lombok.Getter;

/**
 * Tipo que define se o cpf do associado é valido ou não para poder votar
 * @author Tarcísio Lima
 *
 */
@Getter
public enum AbleToVoteTypeEnum {

	ABLE_TO_VOTE,
    UNABLE_TO_VOTE;
	
}
