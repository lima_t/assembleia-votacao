package br.com.tarcisio.infrastructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import lombok.RequiredArgsConstructor;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * Configuração Swagger
 * 
 * @author Tarcísio Lima
 */
@Configuration
@EnableSwagger2
@RequiredArgsConstructor
public class SwaggerConfiguration extends WebConfiguration{
	
	private final Environment environment;

	/**
	 * Cria Docket bean, seu método select () retorna uma instância de
	 * ApiSelectorBuilder, que fornece uma maneira de controlar os endpoints
	 * expostos pelo Swagger.
	 * 
	 * @return
	 */
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("br.com.tarcisio"))			
				.build()
				.apiInfo(this.getApiInfo());
	}
	
	/**
	 * API info
	 * @return
	 */
	private ApiInfo getApiInfo() {
	    return new ApiInfoBuilder()
	            .title("API - " + environment.getProperty("spring.application.name"))
	            .description(environment.getProperty("spring.application.description"))
	            .version(environment.getProperty("spring.application.version"))
	            .license("Licença Pública Geral GNU (GPL)")
	            .licenseUrl("http://licencas.softwarelivre.org/gpl-3.0.pt-br.md")
	            .contact(new Contact(
	            		environment.getProperty("spring.application.author"), 
	            		"https://www.linkedin.com/in/tarcisio-lima/", 
	            		environment.getProperty("spring.application.contact-mail")))
	            .build();
		}
}
