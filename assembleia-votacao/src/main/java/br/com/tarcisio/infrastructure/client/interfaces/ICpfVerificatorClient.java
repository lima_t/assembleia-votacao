package br.com.tarcisio.infrastructure.client.interfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.tarcisio.infrastructure.model.AbleToVoteModel;

/**
 * Client do Feign para acessar a API do heroku com o validador de CPFs
 * @author Tarcísio Lima
 *
 */
@FeignClient(name = "${heroku-api.name}", url = "${heroku-api.url}")
public interface ICpfVerificatorClient {
	
	@GetMapping(value = "/users/{cpf}", consumes = MediaType.APPLICATION_JSON_VALUE)
    AbleToVoteModel isAbleToVoteByCPF(@PathVariable String cpf);

}
