package br.com.tarcisio.infrastructure.model;

import java.io.Serializable;

import br.com.tarcisio.infrastructure.enums.AbleToVoteTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Model para emcapsular o valor que indentifica se o cpf do associado pode ou não votar
 * @author Tarcísio Lima
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AbleToVoteModel implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private AbleToVoteTypeEnum ableToVoteType;

}
