package br.com.tarcisio.associated.application.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import br.com.tarcisio.associated.application.dto.AssociatedRequest;
import br.com.tarcisio.associated.application.dto.AssociatedResponse;
import br.com.tarcisio.associated.application.service.interfaces.IAssociatedService;
import br.com.tarcisio.associated.domain.repository.AssociatedRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service associado
 * @author Tarcísio Lima
 */
@Service
@AllArgsConstructor
@Slf4j
public class AssociatedService implements IAssociatedService{
	
	private final AssociatedRepository associatedRepository;

	/**
	 * Service com as regras de negócio para adicionar novo associado
	 */
	@Override
	public AssociatedResponse addAssociated(AssociatedRequest request) {
		
		log.info("Cadastrando novo associado: "+request.toString());
		
		return new AssociatedResponse(associatedRepository.save(request.toEntity()));
		
	}

	/**
	 * Service com as regras de negócio para busca de um associado por ID
	 */
	@Override
	public AssociatedResponse findById(Long id) {
		
		log.info("Buscando associado pelo ID = "+id.toString());
		
		return new AssociatedResponse(associatedRepository.getById(id));
	}

	/**
	 * Service com as regras de negócio para busca de todos associados
	 */
	@Override
	public List<AssociatedResponse> findAll(Sort sort) {

		log.info("Buscando todos associados por ordem = "+sort.toString());
		
		return associatedRepository.findAll(sort)
				.stream()
				.map(AssociatedResponse::new)
				.collect(Collectors.toList());
	}

}
