package br.com.tarcisio.associated.application.dto;

import br.com.tarcisio.associated.domain.entity.Associated;
import lombok.Data;

/**
 * Objeto representacional de saída da pauta
 * @author Tarcísio Lima
 */
@Data
public class AssociatedResponse {

	private Long id;
	private String name;
	private String cpf;
	
	/**
	 * Construtor de entity em objeto representacional de saída
	 * @param schedule
	 */
	public AssociatedResponse(Associated associated) {
		this.id = associated.getId();
		this.name = associated.getName();
		this.cpf = associated.getCpf();
	}
}
