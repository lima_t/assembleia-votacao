package br.com.tarcisio.associated.domain.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tarcisio.associated.domain.entity.Associated;

public interface AssociatedRepository extends JpaRepository<Associated, Long> {
	
	/**
	 * Busca associado pelo CPF
	 * @param cpf
	 * @return
	 */
	Optional<Associated> findByCpf(String cpf);

}
