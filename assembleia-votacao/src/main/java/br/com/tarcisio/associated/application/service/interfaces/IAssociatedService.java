package br.com.tarcisio.associated.application.service.interfaces;

import java.util.List;

import org.springframework.data.domain.Sort;

import br.com.tarcisio.associated.application.dto.AssociatedRequest;
import br.com.tarcisio.associated.application.dto.AssociatedResponse;

/**
 * Interface associado
 * @author Tarcísio Lima
 */
public interface IAssociatedService {
	
	/**
	 * Template método inserir associado
	 * @param schedule
	 * @return
	 */
	AssociatedResponse addAssociated(AssociatedRequest request);

	/**
	 * Template buscar associado por ID
	 * @param id
	 * @return
	 */
	AssociatedResponse findById(Long id);

	/**
	 * Template buscar todos os associados
	 * @param sort
	 * @return
	 */
    List<AssociatedResponse> findAll(Sort sort);
}
