package br.com.tarcisio.associated.controller.rest;

import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.tarcisio.associated.application.dto.AssociatedRequest;
import br.com.tarcisio.associated.application.dto.AssociatedResponse;
import br.com.tarcisio.associated.application.service.interfaces.IAssociatedService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;

/**
 * Controller da pauta
 * @author Tarcísio Lima
 */
@RequestMapping("/v1/associates")
@RestController
@RequiredArgsConstructor
@Api("Endpoints para o recurso dos associados")
public class AssociatedRestController {

	private final IAssociatedService associatedService;
	
	/**
	 * Busca todos os associados
	 * @param sort
	 * @return
	 */
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AssociatedResponse>> findAll(@RequestParam(required = false, defaultValue = "ASC") String sort) {
		
        var associates = associatedService.findAll(Sort.by(Sort.Direction.fromString(sort), "name"));
        
        return associates.isEmpty() ? new ResponseEntity<>(HttpStatus.NO_CONTENT) : new ResponseEntity<>(associates, HttpStatus.OK);
    }

	/**
	 * Busca o associado pelo ID
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AssociatedResponse> findById(@PathVariable Long id) {
		
		return new ResponseEntity<>(associatedService.findById(id), HttpStatus.OK);
	}
	
	/**
	 * Cadastra uma novo associado
	 * @param pautaRequest
	 * @return
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AssociatedResponse> addAssociated(@RequestBody AssociatedRequest request) {
		
        return new ResponseEntity<>(associatedService.addAssociated(request), HttpStatus.CREATED);
    }

}
