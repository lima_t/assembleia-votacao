package br.com.tarcisio.associated.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Representa o associado
 * @author Tarcísio Lima
 */
@Entity
@Table(name = "EN_ASSOCIATED", schema = "ASSEMBLY")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Associated implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NAME", nullable = false, length = 200)
	private String name;
	
	@Column(name = "CPF", nullable = false, length = 11, unique = true)
	private String cpf;

}
