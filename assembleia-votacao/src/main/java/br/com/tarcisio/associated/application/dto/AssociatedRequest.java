package br.com.tarcisio.associated.application.dto;

import br.com.tarcisio.associated.domain.entity.Associated;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Objeto representacional de entrada do associado
 * @author Tarcísio Lima
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class AssociatedRequest {

	private String name;
	private String cpf;
	
	/**
	 * Transforma o request objeto representacional para entity
	 * @return
	 */
	public Associated toEntity() {
		return new Associated(null, name, cpf);
	}
	
}
