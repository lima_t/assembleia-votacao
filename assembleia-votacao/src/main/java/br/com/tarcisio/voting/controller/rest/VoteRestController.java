package br.com.tarcisio.voting.controller.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.tarcisio.voting.application.dto.VoteRequest;
import br.com.tarcisio.voting.application.dto.VoteResponse;
import br.com.tarcisio.voting.application.service.interfaces.IVoteService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;

/**
 * Controller da votação
 * @author Tarcísio Lima
 */
@RequestMapping("/v1/votes")
@RestController
@RequiredArgsConstructor
@Api("Endpoints para o recurso da votação")
public class VoteRestController {
	
	private final IVoteService voteService;
	
	/**
	 * 
	 * @param request
	 * @return
	 */
	@PostMapping(produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VoteResponse> addVote(@RequestBody VoteRequest request) {
		
		return new ResponseEntity<>(voteService.addVote(request), HttpStatus.CREATED);
    }

}
