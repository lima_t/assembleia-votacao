package br.com.tarcisio.voting.domain.entity.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum VoteTypeEnum {
	
	YES("YES"), 
	NO("NO");
	
	private String ordinalValue;
	
}
