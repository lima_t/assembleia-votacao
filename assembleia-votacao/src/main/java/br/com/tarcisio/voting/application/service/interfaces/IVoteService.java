package br.com.tarcisio.voting.application.service.interfaces;

import java.util.Map;

import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.voting.application.dto.VoteRequest;
import br.com.tarcisio.voting.application.dto.VoteResponse;
import br.com.tarcisio.voting.domain.entity.enums.VoteTypeEnum;

/**
 * Interface votação
 * @author Tarcísio Lima
 */
public interface IVoteService {
	
	/**
	 * Template da função que salva o voto do associado na pauta
	 * @param request
	 * @return
	 */
	VoteResponse addVote(VoteRequest request);
	
	/**
	 * Template da função que realiza a contagem dos votos
	 * @param scheduleToCount
	 * @return
	 */
	Map<VoteTypeEnum, Long> countVotesFromSchedule(Schedule scheduleToCount);

}
