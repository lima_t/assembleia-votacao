package br.com.tarcisio.voting.application.dto;

import br.com.tarcisio.associated.domain.entity.Associated;
import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.voting.domain.entity.Vote;
import br.com.tarcisio.voting.domain.entity.enums.VoteTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Objeto representacional de entrada do voto
 * @author Tarcísio Lima
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class VoteRequest {
	
	private String cpf;
	private Long schedule;
	private VoteTypeEnum voteType;
	
	/**
	 * Transforma o request objeto representacional para entity
	 * @return
	 */
	public Vote toEntity(Long associatedId) {
		return new Vote(
				null, 
				new Associated(associatedId, null, null),
				new Schedule(schedule, null, null, null, null, null, null),
				voteType);
	}
	
}
