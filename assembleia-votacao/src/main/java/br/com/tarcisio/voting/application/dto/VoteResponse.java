package br.com.tarcisio.voting.application.dto;

import br.com.tarcisio.associated.domain.entity.Associated;
import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.voting.domain.entity.Vote;
import br.com.tarcisio.voting.domain.entity.enums.VoteTypeEnum;
import lombok.Data;

/**
 * Objeto representacional de saída do voto na votação
 * @author Tarcísio Lima
 */
@Data
public class VoteResponse {
	
	private Long id;
	private Associated associated;	
	private Schedule schedule;
	private VoteTypeEnum voteType;	

	public VoteResponse(Vote vote) {
		this.id = vote.getId();
		this.associated = vote.getAssociated();
		this.schedule = vote.getSchedule();
		this.voteType = vote.getVoteType();
	}	
}
