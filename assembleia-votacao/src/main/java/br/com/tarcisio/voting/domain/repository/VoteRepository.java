package br.com.tarcisio.voting.domain.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.tarcisio.associated.domain.entity.Associated;
import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.voting.domain.entity.Vote;

public interface VoteRepository extends JpaRepository<Vote, Long> {

	/**
	 * Verifica se o associado já votou em uma pauta
	 * @param schedule
	 * @param associated
	 * @return
	 */
	Optional<Vote> findByScheduleAndAssociated(Schedule schedule, Associated associated);
	
	/**
	 * Retorna lista de votos a partir de uma pauta
	 * @param schedule
	 * @return
	 */
	List<Vote> findBySchedule(Schedule schedule);
	
}
