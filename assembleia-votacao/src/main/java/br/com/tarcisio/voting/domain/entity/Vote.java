package br.com.tarcisio.voting.domain.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import br.com.tarcisio.associated.domain.entity.Associated;
import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.voting.domain.entity.enums.VoteTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Representa os votos
 * @author Tarcísio Lima
 */
@Entity
@Table(name = "EN_VOTE", schema = "ASSEMBLY")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Vote implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")	
	private Long id;
	
	@JoinColumn(name = "ID_ASSOCIATED", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Associated associated;
	
	@JoinColumn(name = "ID_SCHEDULE", nullable = false)
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	private Schedule schedule;
	
	@Column(name = "VOTE_TYPE", nullable = false)
	@Enumerated(EnumType.STRING)
	private VoteTypeEnum voteType;

}
