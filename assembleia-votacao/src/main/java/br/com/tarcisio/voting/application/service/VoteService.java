package br.com.tarcisio.voting.application.service;

import java.time.LocalTime;
import java.util.Map;
import java.util.Optional;

import org.springframework.stereotype.Service;

import br.com.tarcisio.associated.domain.entity.Associated;
import br.com.tarcisio.associated.domain.repository.AssociatedRepository;
import br.com.tarcisio.infrastructure.client.facade.CpfVerificatorFacade;
import br.com.tarcisio.infrastructure.enums.ErrorMessagesEnum;
import br.com.tarcisio.infrastructure.exception.BusinessException;
import br.com.tarcisio.schedule.domain.entity.Schedule;
import br.com.tarcisio.schedule.domain.entity.enums.ScheduleStatusEnum;
import br.com.tarcisio.schedule.domain.repository.ScheduleRepository;
import br.com.tarcisio.voting.application.dto.VoteRequest;
import br.com.tarcisio.voting.application.dto.VoteResponse;
import br.com.tarcisio.voting.application.service.interfaces.IVoteService;
import br.com.tarcisio.voting.domain.entity.Vote;
import br.com.tarcisio.voting.domain.entity.enums.VoteTypeEnum;
import br.com.tarcisio.voting.domain.repository.VoteRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

/**
 * Service votação
 * @author Tarcísio Lima
 */
@Service
@AllArgsConstructor
@Slf4j
public class VoteService implements IVoteService {

	private final VoteRepository voteRepository;
    private final AssociatedRepository associatedRepository;
    private final ScheduleRepository scheduleRepository;
    
    private final CpfVerificatorFacade cpfVerificatorFacade;
	
	/**
	 * Insere o voto do associado
	 */
	@Override
	public VoteResponse addVote(VoteRequest request) {
		
		log.info("Cadastrando novo voto:  {}", request.toString());
		
		var associated = associatedRepository.findByCpf(request.getCpf());
		var schedule = scheduleRepository.findById(request.getSchedule());
		
		validateScheduleSessionAbility(associated, schedule);
		
		var voted = voteRepository.findByScheduleAndAssociated(schedule.get(), associated.get());
		
		boolean cpfIsValid  = cpfVerificatorFacade.isValidCPF(request.getCpf());
		boolean isAbleToVote = voted.isEmpty();
		
		//Validar se o cpf é válido e se ainda não votou
		if(isAbleToVote && cpfIsValid) {
			
			log.info("O associado está habilitado para votar...");
			
			var vote = new Vote(null, associated.get(), new Schedule(request.getSchedule()), request.getVoteType());
			
			return new VoteResponse(voteRepository.save(vote));
			
		} else {
			throw new BusinessException(ErrorMessagesEnum.ASSOCIATED_CAN_NOT_VOTE);
		}
	}

	/**
	 * Valida os pré-requisitos de votação
	 * @param associated
	 * @param schedule
	 */
	private void validateScheduleSessionAbility(Optional<Associated> associated, Optional<Schedule> schedule) {
		
		log.info("Validando pré-requisitos para votação, verificando entidades");
		
		if(associated.isEmpty()) {
			throw new BusinessException(ErrorMessagesEnum.ASSOCIATED_NOT_FOUND);
			
		} else if(schedule.isEmpty()) {
			throw new BusinessException(ErrorMessagesEnum.SCHEDULE_NOT_FOUND);
			
		} else if(schedule.get().getStatus() == ScheduleStatusEnum.FINISHED) {
			throw new BusinessException(ErrorMessagesEnum.SCHEDULE_ALREADY_FINISHED);
			
		} else if(LocalTime.now().isAfter(schedule.get().getEndTime())) {
			
			log.info("Atualizando status da pauta para finalizado.");			
			scheduleRepository.updateScheduleStatusFinished(schedule.get().getId());
			
			throw new BusinessException(ErrorMessagesEnum.SCHEDULE_ALREADY_FINISHED);
		}
	}

	/**
	 * Realiza contagem de votos a partir de um pauta
	 */
	@Override
	public Map<VoteTypeEnum, Long> countVotesFromSchedule(Schedule scheduleToCount) {
		
		log.info("Realizando a contagem dos votos");
		
		var votesFromSchedule = voteRepository.findBySchedule(scheduleToCount);
		
		Long yes = votesFromSchedule.stream().filter(vote -> vote.getVoteType() == VoteTypeEnum.YES).count();
		Long no = votesFromSchedule.stream().filter(vote -> vote.getVoteType() == VoteTypeEnum.NO).count();
		
		log.info("Total de votos SIM: {} | Total de votos não: {}", yes.toString(), no.toString());
		
        return Map.of(VoteTypeEnum.YES, yes, VoteTypeEnum.NO, no);
	}
	
	
	

}
